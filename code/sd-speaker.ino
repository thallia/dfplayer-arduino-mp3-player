/*

Dara Klinkner, 2018
Birthday present for my boyfriend.
A bunch of songs are loaded onto an SD card, the arduino will play them on the speaker attached.

Notes:

 - DFPlayer MUST be connected to the same power supply as the arduino!
 - SD Card needs to have FAT format
 - you can have music files in the root directory, must be in xxxx.mp3 format (don't worry about wav)
 - make sure TX and RX are connected to the corresponding RX and TX on the DFPlayer

*/

// ************************
//   Libraries
// ************************

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h" // music library
#include "LiquidCrystal.h" // display library


// ************************
//   Variables & Inits
// ************************

SoftwareSerial mySerial(10, 11); // RX, TX DFPlayerMini_Fast
DFRobotDFPlayerMini music;
LiquidCrystal lcd(12, 13, 5, 4, 3, 2); // initializes LCD object to "lcd"

int songNumber = 1; // start on song 1
int currentVolume;

int playPause = 6; // pin 6

boolean isPlaying = false;
boolean currentState;


// **********************
//   Setup
// **********************
void setup(){

  pinMode(playPause, INPUT_PULLUP); // button for playing/pausing
  pinMode(isPlaying, INPUT); // for busy signal

  Serial.begin(9600); // for debugging
  mySerial.begin(9600);

  if (!music.begin(mySerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true){
      delay(0);
    }
  }
  Serial.println(F("\nDFPlayer Mini online."));

  music.volume(15); // set the initial volume
  music.play(1); // start playing the first song
  isPlaying = true;

  lcd.begin(16, 2); // tells how many columns and rows for the LCD screen
  lcd.print("Song: ");
  lcd.print(songNumber);
  lcd.print("/70");
  lcd.setCursor(0,1);
  lcd.print("I love you Zack!");

  music.enableLoopAll(); // this plays through all the songs in the SD card :)

}

// **************************
//  Main Loop
// **************************

void loop(){

  if (digitalRead(playPause) == LOW) { // pause the music if playing, and play if paused
    if (isPlaying) {
        music.pause();
        isPlaying = false;
        Serial.println("Paused..");
    } else {
        isPlaying = true;
        music.start();
        Serial.println("Playing..");
    }
  }

  delay(1000);
  songNumber = music.readCurrentFileNumber(); // update songNumber based on current song
  updateSongDisplay(songNumber);


}

// ************************
//  User Defined Functions
// ************************

void updateSongDisplay(int song){
  lcd.setCursor(0,0);
  lcd.print("Song: ");
  lcd.print(song);
  lcd.print("/70 ");
  //Serial.print(songNumber);
}
